package com.example.simplefragments;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

public class StaticFragments extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_static_fragments);
        Button btnSend = (Button) findViewById(R.id.button1);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //การติดต่อกับ UI ใน Fragment จาก Activity
                Fragment frag = getFragmentManager().findFragmentById(R.id.fragment_one);
                TextView txtFrag1 = (TextView) frag.getView().findViewById(R.id.textViewOne1);
                txtFrag1.setText("Message from Activity");
                //Send data from activity to fragment
                Bundle bundle = new Bundle();
                bundle.putString("edttext", "From Activity");
                // set Fragmentclass Arguments
                FragmentOne fragobj = new FragmentOne();
                fragobj.setArguments(bundle);
            }
        });
    }

}

